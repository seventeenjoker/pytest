from pytest import fixture
from selenium import webdriver


@fixture(scope='session')
def chrome_browser():
    yield webdriver.Chrome()

    #teardown
    print("I am tearing down this browser")
